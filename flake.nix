{
  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
  };

  outputs = {nixpkgs, ... }: let
    system = "x86_64-linux";
    validation-layer-overlay = final: prev: {
      vulkan-validation-layers = prev.vulkan-validation-layers.overrideAttrs(old: {
        setupHook = pkgs.writeText "setup-hook" "";
      });
    };

    pkgs = import nixpkgs { inherit system; overlays = [ validation-layer-overlay ]; };
    libs = with pkgs; [
      vulkan-tools
      vulkan-loader
      vulkan-headers
      vulkan-tools-lunarg
      glfw
      glm
      glslang
      shaderc
    ];
  in rec {
    inherit (pkgs) mkShell stdenv;

    devShell."${system}" = mkShell {
      # included in the devShell for language server / etc.
      buildInputs = with pkgs; [
        defaultPackage."${system}"
      ] ++ libs;
      shellHook = ''
        # link the compile commands ( for ccls / clang )
        if [ ! -f compile_commands.json ]; then
          ln -s result/compile_commands.json
        fi

        # import the validation layer path to XDG_DATA_DIRS
        export XDG_DATA_DIRS=${pkgs.vulkan-validation-layers}/share:$XDG_DATA_DIRS

        # compile the shaders
        (
          cd ./shaders
          sh compile.sh
        )
      '';
    };

    defaultPackage."${system}" = stdenv.mkDerivation rec {
      name = "main";
      version = "0.1";
      src = ./.;
      nativeBuildInputs = with pkgs; [
        cmake
        vulkan-validation-layers # see https://github.com/NixOS/nixpkgs/pull/106085
      ];

      buildInputs = with pkgs; [
        tree
      ] ++ libs;

      cmakeFlags = [
        "-DNAME=${name}"
        "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
        "-DCMAKE_BUILD_TYPE=Debug"
      ];

      makeTarget = "all";
      enableParallelBuilding = true;

      installPhase = ''
        mkdir -p $out/bin
        cp ${name} $out/bin
        tree
        cp compile_commands.json $out
      '';
    };
  };
}
